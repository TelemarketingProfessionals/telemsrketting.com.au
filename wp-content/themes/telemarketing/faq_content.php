<html><head><link type="text/css" rel="stylesheet" href="css/2.css">
<style type="text/css">
&lt;!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
--&gt;
</style><title>FAQ:Telemarketing Professionals provides answers to commonly asked questions</title>
<meta content="Telemarketing Professionals provides the highest quality phone marketing using Australian inhouse call agents, we provide a complete range of B2B outbound telemarketing services from Sydney in NSW throughout Australia and New Zealand including Appointment Setting, Lead Generation, Telesales, Event/Seminar Invitation and Confirmation, Database Cleansing, List Brokering,Database, Customer Service, Research Calls, Survey Calls and Win Back Calls, Intelligence Gathering, IT Lead Generation, B2B Lead Generation. " name="Description">
<meta content="Telemarketing Professionals only use OZ callers of the highest caliber to ensure you get the best results. We operate from Sydney and provide outbound calls to Australian and New Zealand. 

We can assist with the following: Lead Generation, Appointment Setting, Telesales, Event Invitation and Confirmation, Seminar Invitation and Confirmation, Database Cleansing and Update, Customer Satisfaction, Research and Survey calls. " name="Description">
</head><body><div>
<li class="standard_text"><strong>How much       experience do you have?</strong><br>
            We have over a decade of sales and lead generation  experience in various industries, targeting business of all sizes and  predominately focusing on the IT industry.<br>
            <br>
            </li>
            <li class="standard_text"><strong>What       experience do your agents have?</strong><br>
            We only recruit sales agents with extensive sales  and direct lead generation experience, we choose  agents that  are best suited to your industry and campaign or recruit agents with experience  in your field<strong>.<br>
            </strong><br>
            </li>
            <li class="standard_text"><strong>Why not       just run my campaign in house?</strong><br>
            The reason many companies outsource this area is  once you take into account the fact that this is not your core competencie,  cost of hiring, training, managing, monitoring, retention, phone call charges,  office space, overheads it quickly becomes evident that outsourcing is often  the best solution. <br>
            <br>
            The fact is that telemarketing is a micro management  industry as everyone hates cold calling and will find any excuse not to. Do you  have the time, patience, resources or inclination to micro manage? If not, then  outsource.<br>
            <br>
            When you do your sums and compare  to other forms of advertising and marketing, telemarketing campaigns are still  the most cost effective form of marketing available today. This is even more so  when combined with email and mail follow up.<br>
            <br>
            </li>
            <li class="standard_text"><strong>Do you take       on B2C campaigns?<br>
            </strong>No. We only focus on the B2B sector<br>
            <br>
            </li>
            <li class="standard_text"><strong>Do you use       overseas call center operators?<br>
            </strong>No. We only use locally Australian based operators  with a clear and easily understood English accent as well as a level of  maturity and industry experience.<br>
            <br>
            </li>
            <li class="standard_text"><strong>Do you do       multilingual overseas campaigns?<br>
            </strong>No. We focus on Australian and New Zealand  calls in English<br>
            <br>
            </li>
            <li class="standard_text"><strong>Minimum       hours I can sign up for?</strong><br>
            Minimum hours you can sign up for:&nbsp; Is a 40 hour pilot. <br>
            <br>
            We have found that it often takes 2 or 3 calls to  get to speak to the correct contact, even then they may say "You�ve caught me at  a bad time can you call me back?".  As a result it takes a couple of weeks of calling to give  a fair indication of likely results.<br>
            <br>
            </li>
            <li class="standard_text"><strong>Guarantee?</strong><br>
            We understand that you are concerned about the  Risk/Reward ratio. We can not give a blanket guarantee as each campaign is  different. Trying to get leads from an office manger will be easier than trying  to get leads from a CEO/CIO/CFO, also depending on the total value and  specialty of your product the need may be small. <br>
            <br>
            We will touch base with you after the first 20 hours  and provide feedback. If you are not satisfied you can pull the trial and the  remaining hours will be refunded. The set-up-fee as well as a purchased marketing list is non-refundable. It  takes a ramp  up period of at least a week to find out who the best contact is, when they  will be available and be able pitch them, and to get into the swing of things, so the longer the campaign the better  the results will be.<br>
            <br>
            </li>
            <li class="standard_text"><strong>SLA (Service Level Agreement)<br>
            </strong>We do not normally offer SLA's however pnce we have completed the pilot, we will have an  idea as to how many leads or appointments have been set. Based on this we can  provide an SLA, if we do not achieve this in  one week we will put on additional hours to make up the short fall in following weekt.  Should you decide not to extend the campaign we can cut our cloth according to  the shortfall and refund the difference? In this model though there should  likewise be a reward for over achievement.<br>
            <br>
            </li>
            <li class="standard_text"><strong>Pay Per       Lead?</strong><br>
            We do not operate under this model.<br>
            <br>
            </li>
            <li class="standard_text"><strong>Marketing       Lists</strong><br>
            We can help provide you with the best marketing list  available for your target market, or you can use your own list.<br>
            <br>
            </li>
            <li class="standard_text"><strong>How long to       get started?<br>
            </strong>Depending on the size of the campaign as well as  scheduling in the required resources anywhere between a 1 to 2 weeks on average. <br>
            <br>
            </li>
            <li class="standard_text"><strong>What type       of payment method do you accept?</strong><br>
            The payment  method of preference is EFT.&nbsp; However, we do understand that some  companies must use a credit card and in that case we will accept a credit card  payment through PayPal.&nbsp;There will be a 3% premium added to the total cost  of services. Direct Debit can also be arranged.<br>
            <br>
            </li>
            <li class="standard_text"><strong>What are       your payment terms?</strong><br>
            Payment terms are up front, prior to commencement of  campaign or pilot. <br>
            <br>
            </li>
            <li class="standard_text"><strong>Reports</strong><br>
            We will provide you with leads and appointments as  acquired daily. We will provide you with regular ongoing feedback as well as a  detailed end of campaign report on completion of the campaign and once full payment has been received. <br>
            </li>
          
<ul>
      <p></p>
          </ul></div></body></html>