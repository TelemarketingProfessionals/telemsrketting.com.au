<?php

/**

 * The template for displaying all pages.

 *

 * This is the template that displays all pages by default.

 * Please note that this is the WordPress construct of pages

 * and that other 'pages' on your WordPress site will use a

 * different template.

 *

 * @package WordPress

 * @subpackage Twenty_Ten

 * @since Twenty Ten 1.0

 */



get_header(); ?>

<?php wp_head();?>



		

			<?php

			/* Run the loop to output the page.

			 * If you want to overload this in a child theme then include a file

			 * called loop-page.php and that will be used instead.

			 */

			//get_template_part( 'loop', 'page' );

			?>

              <link href="<?php bloginfo('template_url'); ?>/css/2.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">



function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}



function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}



function MM_findObj(n, d) { //v4.01

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && d.getElementById) x=d.getElementById(n); return x;

}



function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}



//-->

</script>

<body onLoad="MM_preloadImages('<?php bloginfo('template_directory');?>/images/consultation_button_s_over.jpg','<?php bloginfo('template_directory');?>/images/download_brochure_button_s_over.jpg')"><div class="services">

  <p><span class="blue_header"><br/>

    OUR SERVICES - Outbound B2B</span>

    <span class="small_content"><br />

  <br />

    </span>

	<span  style=" position: relative;top: -14px;">

<?php dynamic_sidebar('secondary-widget-area'); ?></p>

</span>

	</div>

<div class="download">
 <table width="287" border="0" align="center" cellpadding="0" cellspacing="0" class="download_buttons_table">

    <tr>

      <td colspan="4" align="right"><img src="<?php bloginfo('template_directory'); ?>/images/spacer.gif" alt="" width="10" height="15" border="0" /></td>

    </tr>

    <tr align="center" valign="middle">

      <td width="270"><img src="<?php bloginfo('template_directory'); ?>/images/spacer.gif" alt="" width="17" height="10" border="0" /></td>

      <td width="270" align="left"><a href="http://telemarketingprofessionals.com.au/wp-content/uploads/2015/10/Telemarketing_Professionals-Brochure.pdf"><img src="<?php bloginfo('template_directory'); ?>/images/download_brochure_button_s.jpg" alt="Download brochure" width="130" height="83" border="0" id="Image1" /></a></td>

      <td ></td> 

      <td width="270" align="left"><a href="<?php echo site_url();?>/contact-us"><img src="<?php bloginfo('template_directory'); ?>/images/consultation_button_s.jpg" alt="Free Consultation" width="130" height="83" border="0" id="Image2" /></a></td>

    </tr>

  </table>

 
</div>            </div>

  <div id="sim_new">

<div style="width:530px; height:200px; margin:0px 0px 0px 12px; float:left; ">
<?php if(function_exists('shs_slider_view')){ shs_slider_view(); } ?>
</div>

</div>

          <div class="frontpage">

         <?php	$my_postid = 5;

	$content_post = get_post($my_postid);

	$content = $content_post->post_content;

	$content = apply_filters('the_content', $content);

	$content = str_replace(']]>', ']]>', $content);

	echo $content; ?>

                    

          </div>

<div>



        <div class="testimonial"><h4 style="color:#214c79">Testimonials</h4><br><?php // if(function_exists('shs_slider_view')){ shs_slider_view(); } ?>

          “<i><span style="color: #162b6b;">“We have experienced an increase in inbound inquiries along with a higher quality of lead, Shaun and Telemarketing Professionals have performed above our expectation all while presenting our company in a polite and professional manner. We would highly recommend using Telemarketing Professionals.”</span> </i>
<b><span style="color: #162b6b;">Ben Gower, Sales Manager, Boston Commercial Service Pty Ltd</span></b>

          <br/>

<div class="read">

<a href="http://telemarketingprofessionals.com.au/testimonials/"><img src="<?php bloginfo('template_directory'); ?>/images/read.png"></a>

</div>

</div>



</div>

<div class="bglogo">

<div class="tite" style="width: 150px;font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; color:#214c79;" >Some of our customers:</div>

<div class="banner1">



<a href="https://www.thebostongroup.com.au/" target="_blank" ><img src="<?php bloginfo('template_directory'); ?>/images/1.png"></a>

<a href="http://www.maximam.com.au/" target="_blank" style="margin:0px 0px 0px 30px;" ><img src="<?php bloginfo('template_directory'); ?>/images/2.png"></a>

<a href="http://www.planwell.net/" target="_blank" style="margin:0px 0px 0px 30px;"><img src="<?php bloginfo('template_directory'); ?>/images/3.png"></a>

<a href="http://www.anz.idc.asia/" target="_blank" style="margin:0px 0px 0px 30px;" ><img src="<?php bloginfo('template_directory'); ?>/images/4.png"></a>

<a href="http://www.tcf.net.au/" target="_blank" style="margin:0px 0px 0px 30px;" ><img src="<?php bloginfo('template_directory'); ?>/images/5.png"></a>





</div>

<div class="banner1">



<a href="http://www.cadinternational.com/cadinternational/" target="_blank" ><img src="<?php bloginfo('template_directory'); ?>/images/cad.gif"></a>

<a href="http://www.letbc.com.au/" target="_blank" style="margin:0px 0px 0px 30px;" ><img src="<?php bloginfo('template_directory'); ?>/images/8.png"></a>

<a href="http://www.noggin.com.au/" target="_blank" style="margin:0px 0px 0px 30px;"><img src="<?php bloginfo('template_directory'); ?>/images/9.png"></a>

<a href="http://www.objective.com/anz/index.html" target="_blank" style="margin:0px 0px 0px 30px;" ><img src="<?php bloginfo('template_directory'); ?>/images/10.png"></a>

<a href="http://www.gcommerce.com.au/" target="_blank" style="margin:0px 0px 0px 30px;" ><img src="<?php bloginfo('template_directory'); ?>/images/11.png"></a>



</div>



</div>



		



			<div class="push"></div>

<div class="footer">

		



<?php// get_sidebar(); ?>

<?php get_footer(); ?>

