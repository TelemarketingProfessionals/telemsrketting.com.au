function WebAnalytics (thisDomain,gaIDCode) {

	try {
		pageTracker = _gat._getTracker(gaIDCode); /* Google Analytics load */
	} catch(err) {}

	waGAOK = (typeof pageTracker != "undefined");
	waGAFileListReg = /[^\/]+\.(doc|docx|eps|jpg|png|svg|xls|ppt|pps|pptx|pdf|xlsx|zip|txt|vsd|vxd|js|css|rar|exe|wma|mov|avi|wmv|mp3)/
	waOK2Track = (waPageHost.indexOf(thisDomain) != -1);
	waHomeDomain = thisDomain;
	if (!waOK2Track)  { waGAOK = false; }
	if (waGAOK) {
		pageTracker._trackPageview(waPagePath+waPageQuery);
		pageTracker._trackPageLoadTime();
	} /* -- ga -- */

} /* WebAnalytics */

function waPageParser() {

	if (document.getElementsByTagName) {
		if (waGAOK) {
			var gaHrefs = document.getElementsByTagName("a");
			for (var i=0;i<gaHrefs.length;i++) {
				try {
					var gaThisProtocol=gaHrefs[i].protocol.toLowerCase();
					var gaThisDomain=gaHrefs[i].hostname.toLowerCase();
					var gaThisPathname=gaHrefs[i].pathname.toLowerCase();
					var gaThisHref=gaHrefs[i].href.toLowerCase();

					var gaThisPathname = (gaThisPathname.charAt(0) == "/") ? gaThisPathname : "/" + gaThisPathname;

					if (gaThisProtocol == "mailto:") {
						waStartListening(gaHrefs[i],"click",waTrackMailto);
						continue;
					}

					if (gaThisProtocol == "javascript:") {
						waStartListening(gaHrefs[i],"click",waTrackScript);
						continue;
					}

					if (gaThisHref.match(waGAFileListReg) && gaThisDomain.indexOf(waHomeDomain)!=-1) {
						waStartListening(gaHrefs[i],"click",waTrackDownloads);
						continue;
					}

					if (gaThisHref.indexOf("http")==0 && gaThisDomain.indexOf(waHomeDomain)==-1) {
						waStartListening(gaHrefs[i],"click",waTrackExternalLinks);
						continue;
					}
				}
				catch(e){ continue; }
			} /* for */
		} /* waGAOK */
	} /* getElementsByTagName */

/* ---------------------------------------- Start MLC website code ----------------------------------------- */

  /* Homepage small feature tracking */

	if (document.getElementById) {
		if (document.getElementById("homegraphic")) {
			var divlist = document.getElementById("homegraphic").getElementsByTagName("div");
			if (divlist.length > 0) {
				for (var i=0; i<divlist.length; i++) {
					if (divlist[i].style.display == "block") {
						var targElement = divlist[i].getElementsByTagName("a");
						if (targElement.length > 0) {
							var advId = waFetchLabel(targElement[0]);
								waEventAdvert(targElement[0]);
							for (var j=0; j<targElement.length; j++) {
									waStartListening(targElement[j],"click",waEventAdvertClick);
							} /* for j */
						} /* targElement > 0 */
					} /* if block */
				} /* for i */
			} /* if divlist */
		} /* if homegraphic */
	}

	/* campaign callout tracking */

	if (document.getElementById && waJQueryOK) {
		var col3 = jQuery("div.cthr");
		if (col3.length > 0) {
			var col3div = col3[0].getElementsByTagName("div");
			var i = 0;
			var col3targ = -1;
			while (i < col3div.length) {
				if (col3div[i].className == "feature") {
					i++;
					if (col3div[i].className == "") { i++; }
				} else {
					col3targ = i; break;
				}
			} /* i */
			if (col3targ > 0) {
				var col3callout = col3div[col3targ].getElementsByTagName("a");
				for (var i=0; i<col3callout.length; i++) {
					advId = waFetchLabel(col3callout[i]);
					waEventAdvert(col3callout[i]);
					waStartListening(col3callout[i],"click",waEventAdvertClick);
				} /* i */
			} /* col3targ */
		} /* col3 */
	}

	/* Spotlight Tracking */

	if (document.getElementById && waJQueryOK) {
		var featureDiv = jQuery("div.feature");
		if (featureDiv.length > 0) {
			for (var i=0; i<featureDiv.length; i++) {
				var featureDivH2 = featureDiv[i].getElementsByTagName("h2");
				if (featureDivH2.length>0) {
					if (featureDivH2[0].innerHTML.toLowerCase().indexOf("spotlight") != -1) {
						var featureDivDiv = featureDiv[i].getElementsByTagName("div");
						for (var j=0; j<featureDivDiv.length; j++) {
							var featureDivDivA = featureDivDiv[j].getElementsByTagName("a");
							for (var k=0; k<featureDivDivA.length; k++) {
								if (featureDivDivA[k].className.indexOf("waNoAutoTag") == -1) {
									var advId = waFetchLabel(featureDivDivA[k]);
									waEventAdvert(featureDivDivA[k]);
									waStartListening(featureDivDivA[k],"click",waEventAdvertClick);
								} /* tag anchor */
							} /* k anchors within div */
						} /* j divs within feature */
					} /* spotlight found */
				} /* h2 */
			} /* i */
		} /* featureDiv */
	}

	/* Dedicated advertising elements */

	if (document.getElementById && waJQueryOK) {
		var adverts = jQuery('div.waTrackedAdvert');
		if (adverts.length > 0) {
			for (var i=0; i<adverts.length; i++) {
				var advertLabel = adverts[i].getAttribute("data-waTrackedAdvertLabel");
				waEventAdvert(advertLabel);

				var advertsA = adverts[i].getElementsByTagName("a");
				for (var j=0; j<advertsA.length; j++) {
					waStartListening(advertsA[j],"click",waEventAdvertClick);
				} /* j */
			} /* i */
		} /* adverts */
	}

/* ---------------------------------------- End MLC website code ----------------------------------------- */

} /* waPageParser */

function waFetchLabel(atag) {
	var images = atag.getElementsByTagName("img");
	var labelText = "";
	for (var i=0; i<images.length; i++) {
		if (typeof images[i].alt != "undefined") if (images[i].alt != "") { labelText = images[i].alt; break }
		if (typeof images[i].title != "undefined") if (images[i].title != "") { labelText = images[i].title; break }
	}
	if (labelText == "") { /* if no alt or title text found */
		labelText = (atag.pathname.charAt(0) == "/") ? atag.pathname : "/" + atag.pathname;
		labelText = labelText.toLowerCase().replace("/mlc/",""); /* MLC ONLY - use path and delete leading /mlc directory */
		labelText = labelText.replace(".html",""); /* remove extension for html pages */
		labelText = labelText.replace(/\//g," "); /* convert all slashes to spaces */
		labelText = labelText.replace(/\%20/g," "); /* convert all %20 to spaces */
	}
	return labelText;
}

function waEventCall(waEventId) {
	if (typeof waEventId.id == "undefined") return true; /* exit if no id tag */
	var waEvent = waEventId.id.toString();
	if (waGAOK) { pageTracker._trackPageview(waPagePath + "/" + waEvent); } /* -- ga -- */
	return true;
}

function waEventCallStr(waEventStr) {
	if (waGAOK) { pageTracker._trackPageview(waPagePath + "/" + waEventStr); } /* -- ga -- */
	return true;
}

function waEventAnchor(eventType,eventLink) {
	pageTracker._trackEvent(eventType,eventLink,waPagePath); /* -- ga -- */
	if (eventType == "File" && waTrackFlagFile) {
		pageTracker._trackPageview("/file/" + eventLink) /* -- ga -- */
	}
}

function waEventAdvert(evnt) {
	if (typeof evnt != "string") {
		var waAdvertRef = waFetchLabel(evnt);
	} else {
		var waAdvertRef = evnt;
	}
	if (waGAOK) waEventAnchor("Onsite Ad Impression",waAdvertRef); /* -- ga -- */
}

function waEventAdvertClick(evnt) {
	if (typeof evnt != "string") {
		var e = (evnt.srcElement)?evnt.srcElement:this;
		while (e.tagName != "A") { e = e.parentNode; }
		var waAdvertRef = waFetchLabel(e);
	} else {
		var waAdvertRef = evnt;
	}
	if (waGAOK) waEventAnchor("Onsite Ad Clickthroughs",waAdvertRef); /* -- ga -- */
}

function waTrackMailto(evnt) {
	var e = (evnt.srcElement)?evnt.srcElement:this;
	while (e.tagName != "A") { e = e.parentNode; }
	var hrefStr = e.href.substring(7);
	if (waGAOK) waEventAnchor("Email",hrefStr);
}

function waTrackScript(evnt) {
	var e = (evnt.srcElement)?evnt.srcElement:this;
	while (e.tagName != "A") { e = e.parentNode; }
	var hrefStr=e.href.substr(11);
	if (waGAOK) waEventAnchor("Script",hrefStr);
}

function waTrackDownloads(evnt) {
	var e = (evnt.srcElement)?evnt.srcElement:this;
	while (e.tagName != "A") { e = e.parentNode; }
	var fileName = e.href.match(waGAFileListReg);
	if (waGAOK) waEventAnchor("File",fileName[0]);
}

function waTrackExternalLinks(evnt) {
	var e = (evnt.srcElement)?evnt.srcElement:this;
	while (e.tagName != "A") { e = e.parentNode; }
	/*if (waGAOK)*/ waEventAnchor("External",e.href);
}

function waStartListening(obj,evnt,func) {
	if (obj.addEventListener) {
		obj.addEventListener(evnt,func,false); /* mozilla */
	}
	else {
		if (obj.attachEvent) {
			obj.attachEvent("on" + evnt,func); /* ie5+ only */
		}
	}
}

function waListenEvent(obj,jsEvent,gaEventCategory,gaEventAction,gaEventLabel) {
	if (waGAOK) {
		if (obj.addEventListener) {
			obj.addEventListener(jsEvent,function(){waTrackEvent(gaEventCategory,gaEventAction,gaEventLabel)},false); /* mozilla */
		}
		else {
			 if (obj.attachEvent) {
				 obj.attachEvent("on" + jsEvent,function(){waTrackEvent(gaEventCategory,gaEventAction,gaEventLabel)}); /* ie5+ only */
			 }
		 }
	}
	return true;
}

function waTrackEvent(gaEventCategory,gaEventAction,gaEventLabel){
	if (waGAOK){
		pageTracker._trackEvent(gaEventCategory,gaEventAction,gaEventLabel);
	}
}


/* ------------- Load time execution and Initialisation ------------- */

var waJQueryOK = (typeof jQuery != "undefined"); /* jquery available */

var host = window.location.hostname;
if (/^(mkb|mka|mkc|mkuat)\.mlc\..+/i.test(host)) {
    var waPageQuery = '';
    if (host.toLowerCase().substring(0,3)=='mkb') {
        var waDomain = host;
        var waGAID="UA-18550539-5";
    } else if (host.toLowerCase().substring(0,3) == 'mka') {
        var waDomain = host;
        var waGAID="UA-18550539-1";
    } else if (host.toLowerCase().substring(0,3) == 'mkc') {
        var waDomain = host;
        var waGAID = "UA-18550539-7";
    } else if (host.toLowerCase().substring(0,3) == 'mku') {
        var waDomain = host;
        var waGAID="UA-18550539-6";
    }
}

if (typeof waPageQuery == "undefined") { waPageQuery = unescape(window.location.search); }

/* -- Option to exclude query string parameters by setting waQueryFilter -- */
if (typeof waQueryFilter != "undefined" && waPageQuery) {
	var waFilterArray = waQueryFilter.split(",");
	var waNewSearch = "";
	waQueryArray = waPageQuery.substring(1,waPageQuery.length).split("&");
	for (var i=0;i<waQueryArray.length;i++) {
		var waQueryArrayItem = waQueryArray[i].split("=");
		var waQueryMatch = false;
		for (var j=0; j<waFilterArray.length; j++) {
			if (waQueryArrayItem[0] == waFilterArray[j]) { waQueryMatch = true; waFilterArray.splice(j,1); break; }
		} /* for j */
		if (!waQueryMatch) { waNewSearch = waAppendQuery(waNewSearch,waQueryArrayItem[0],waQueryArrayItem[1]); }
	} /* for i */
	waPageQuery = waNewSearch;
}

if (typeof waPageHref == "undefined") { waPageHref = window.location.href; }
if (typeof waPageHost == "undefined") { waPageHost = window.location.hostname.toLowerCase(); }
if (typeof waPagePath == "undefined") { waPagePath = window.location.pathname.replace(/\/$/,""); }

if (typeof waTrackFlagFile == "undefined") { waTrackFlagFile = false; } /* pre-define waTrackFlagFile to true if files are to be tracked as pages for GA */

if (typeof waDomain == "undefined") { waDomain = "mlc.com.au"; }
if (typeof waGAID == "undefined") { waGAID = "UA-1614577-1"; }

WebAnalytics(waDomain,waGAID);

waPageParser();