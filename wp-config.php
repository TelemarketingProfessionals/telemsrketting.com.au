<?php

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'dnaactiv_new');

/** MySQL database username */
define('DB_USER', 'dnaactiv_new');

/** MySQL database password */
define('DB_PASSWORD', 'Newdata@123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[do.rqw8rCvsi+nF[+*sxji),kx!HMi|~#ou?Z-P&d ]qT|jxyB`PpZX10l,-w10');
define('SECURE_AUTH_KEY',  '<U~?j)|r&8ZwNdZU0v6}4-!U-{ocz=Hb=?iL!dA&>|-54p!%Xy}9B5WiC^/E{bD+');
define('LOGGED_IN_KEY',    'nS|z}J~i1I]ua0h[yrH)=t,Jd7@#$p;DrknJNM.qR2FgS`pm3#<9$3/~rLai*fmr');
define('NONCE_KEY',        '|za&?C] RkaRWtDU]P;QEja5W!K/]5Wa-Nt%U2=mt-@rF+U-Sl:*mNFe*{[=;I+2');
define('AUTH_SALT',        'UJRd&%G)4pf0d|F(uO1>hRTXF-An ZwOX3-v;;[PtZ[=#v{b1D]!<ZGQJ>`5GF<7');
define('SECURE_AUTH_SALT', '9nY}MKuu]Ob_Y@|X7TJn(mrJA55%Z|Y;w~gkwT>b.{Wb#vL_i9MrDK&4^n,n83; ');
define('LOGGED_IN_SALT',   '0;kSkQW>MP|E*K*2Zm$O}e9sLs=9!I;sY<T=tIl:a|-7GDB2g.lh0g7D^^JiNrN5');
define('NONCE_SALT',       '~~dS]GdEY,`,+?^@h)GHfI.{_nw~uBc-HV=lUMO?<x|oz+ab{vE>+__Dc0kw>o ]');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

define( 'AUTOSAVE_INTERVAL', 300 );
define( 'WP_POST_REVISIONS', 5 );
define( 'EMPTY_TRASH_DAYS', 7 );
define( 'WP_CRON_LOCK_TIMEOUT', 120 );
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');